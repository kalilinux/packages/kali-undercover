kali-undercover (2023.4.2) kali-dev; urgency=medium

  [ Steev Klimaszewski ]
  * d/upstream: Add upstream metadata

  [ Daniel Ruiz de Alegría ]
  * Ensure xfce4-panel runs again

 -- Daniel Ruiz de Alegría <daniruiz@kali.org>  Thu, 23 May 2024 09:37:04 +0200

kali-undercover (2023.4.1) kali-dev; urgency=medium

  * Updates for whiskermenu

 -- Daniel Ruiz de Alegría <daniruiz@kali.org>  Fri, 24 Nov 2023 11:23:35 +0100

kali-undercover (2023.4.0) kali-dev; urgency=medium

  * Add missing px units in GTK theme

 -- Daniel Ruiz de Alegría <daniruiz@kali.org>  Mon, 09 Oct 2023 08:44:25 +0200

kali-undercover (2023.1.0) kali-dev; urgency=medium

  [ Steev Klimaszewski ]
  * Add d/watch

  [ Kali Janitor ]
  * Update lintian override info format in d/kali-undercover.lintian-overrides
    on line 5.
  * Update standards version to 4.6.1, no changes needed.

  [ Daniel Ruiz de Alegría ]
  * Update packaging
  * Update for Xfce 4.18
  * Fix file format of packaged panel profiles
  * Recover config file for datetime panel plugin
  * Remove duplicated date settings
  * Sync python scripts with xfce4-panel-profiles source code
  * Fix firefox always uses dark theme
  * Change mousepad's color-scheme if the default is selected

 -- Daniel Ruiz de Alegría <daniruiz@kali.org>  Tue, 10 Jan 2023 18:15:56 +0100

kali-undercover (2021.4.0) kali-dev; urgency=medium

  [ Ben Wilson ]
  * Update email address
  * Add Uploaders
  * Update control

  [ DяA ]
  * Add README.md

  [ Daniel Ruiz de Alegría ]
  * Improve qt5 theme
  * Compress icon theme
  * Improve whiskermenu theme
  * Improve whiskermenu gtk theme
  * Compress icons with pngquant

 -- Daniel Ruiz de Alegría <daniruiz@kali.org>  Wed, 01 Dec 2021 10:58:52 +0100

kali-undercover (2021.1.2) kali-dev; urgency=medium

  * Version bump to fix upload

 -- Arnaud Rebillout <arnaudr@kali.org>  Fri, 05 Feb 2021 23:57:21 +0700

kali-undercover (2021.1.1) kali-dev; urgency=medium

  [ Daniel Ruiz de Alegría ]
  * Reduce font size to panel date plugin
  * Add icons for Xfce 4.16
  * Update whiskermenu theme
  * Update whiskermenu settings
  * Remove executable permission from images
  * Whiskermenu disable menu hierarchy to enable menu with icons
  * Add extra favorites to whiskermenu
  * Add missing icons for the logout dialog

  [ Arnaud Rebillout ]
  * Update standards version to 4.5.1, no changes needed

 -- Arnaud Rebillout <arnaudr@kali.org>  Fri, 05 Feb 2021 21:23:48 +0700

kali-undercover (2021.1.0) kali-dev; urgency=medium

  * Recover menu logout and switch-user buttons
  * Minify png files

 -- Daniel Ruiz de Alegría <daniruiz@kali.org>  Fri, 18 Dec 2020 12:06:24 +0100

kali-undercover (2020.4.0) kali-dev; urgency=medium

  * Remove old unneeded code
  * Improve the position of the title buttons of the gtk3 theme
  * Add custom qt5 theme settings
  * Add MSDOS like prompt to zsh
  * Add custom qterminal theme settings
  * Use zsh as the undercovered prompt if the command is run from zsh

 -- Daniel Ruiz de Alegría <daniruiz@kali.org>  Mon, 28 Sep 2020 10:47:30 +0200

kali-undercover (2020.2.0) kali-dev; urgency=medium

  * Hide plank panel if it is running
  * Replace xfce clock plugin with datetime
  * Increase panel height to 44px

 -- Daniel Ruiz de Alegría <daniruiz@kali.org>  Sun, 01 Mar 2020 17:15:17 +0100

kali-undercover (2020.1.2) kali-dev; urgency=medium

  * Fix panel directory menu base-directory
  * Move xfwm4 and xsettings settings manager to desktopconfig.py
  * Add missing xfwm4 and xsettings undercover properties
  * Darken xfwm4 theme inactive window title color
  * Reduce separation between window icon and label
  * Add new panel launchers
  * Hide whiskermenu kali shortcuts
  * Add 'undercovered' kali-menu app icon

 -- Daniel Ruiz de Alegría <daniruiz@kali.org>  Fri, 03 Jan 2020 13:39:53 +0100

kali-undercover (2020.1.1) kali-dev; urgency=medium

  * Add missing printf new-line character
  * Show MS-DOS like prompt when launched from terminal

 -- Daniel Ruiz de Alegría <daniruiz@kali.org>  Mon, 09 Dec 2019 18:48:54 +0100

kali-undercover (2020.1.0) kali-dev; urgency=medium

  [ Daniel Ruiz de Alegría ]
  * Check if Xfce desktop is used
  * Simulate MS-DOS prompt

  [ Raphaël Hertzog ]
  * Use full path to modify ~/.bashrc
  * Print an error message on stderr too when not in Xfce
  * Add dependency on procps for pgrep

 -- Raphaël Hertzog <raphael@offensive-security.com>  Tue, 03 Dec 2019 14:25:01 +0100

kali-undercover (2019.4.3) kali-dev; urgency=medium

  * Add Depends to fonts-liberation

 -- Raphaël Hertzog <raphael@offensive-security.com>  Fri, 22 Nov 2019 08:06:08 +0100

kali-undercover (2019.4.2) kali-dev; urgency=medium

  [ Daniel Ruiz de Alegría ]
  * Add Windows 10 background
  * Enable window buttons layout modification
  * Improve script for saving custom user files; Include ~/.face

 -- Raphaël Hertzog <raphael@offensive-security.com>  Thu, 21 Nov 2019 14:14:01 +0100

kali-undercover (2019.4.1) kali-dev; urgency=medium

  [ Daniel Ruiz de Alegría ]
  * Hide thunar root user warning

 -- Raphaël Hertzog <raphael@offensive-security.com>  Wed, 20 Nov 2019 13:40:02 +0100

kali-undercover (2019.4.0) kali-dev; urgency=low

  [ Daniel Ruiz de Alegría ]
  * Initial Release.

  [ Raphaël Hertzog ]
  * Add psmisc to Depends for killall
  * Don't install LICENSE.md but update the copyright file
  * Add more XFCE plugins that are used in the cover mode
  * Rewrite the long description
  * Use dh-python for python 3 dependencies

 -- Daniel Ruiz de Alegría <daniruiz@kali.org>  Fri, 15 Nov 2019 10:58:22 +0100
